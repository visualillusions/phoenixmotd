package net.visualillusionsent.phoenix.motd;

/**
 * Message Of The Day Owner interface<p/>
 * Used to associate either the Server or a Plugin as the owner of a MOTDListener
 *
 * @author Jason Jones (darkdiplomat)
 */
public interface MOTDOwner {

    /**
     * Returns the name of the MOTDOwner (whether a Plugin or another Implementing System)
     *
     * @return owner name
     */
    String getName();

}
