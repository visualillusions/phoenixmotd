package net.visualillusionsent.phoenix.motd;

/**
 * Message Of The Day parser<p/>
 * Calls the logic of the key to be replaced</p>
 * *INTERNAL USE*
 *
 * @author Jason Jones (darkdiplomat)
 */
abstract class MOTDParser {
    private final String key;
    private final MOTDOwner owner;

    MOTDParser(String key, MOTDOwner owner) {
        this.key = key;
        this.owner = owner;
    }

    final String key() {
        return key;
    }

    final MOTDOwner getOwner() {
        return owner;
    }

    abstract String parse(MOTDReceiver receiver) throws Exception;
}
