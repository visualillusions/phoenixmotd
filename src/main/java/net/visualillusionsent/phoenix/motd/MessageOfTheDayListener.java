package net.visualillusionsent.phoenix.motd;

/**
 * Plugin Message Of The Day Listener<br/>
 * A implementing source should use this as their means to handle MOTD Variables<p/>
 *
 * Example:<br/>
 * <pre>
 * <code>
 * {@literal @}MOTDKey("{name}")
 * public final String receiverName(MOTDReceiver motdReceiver) {
 *     return motdReceiver.getName();
 * }
 * </code>
 * </pre>
 *
 * @author Jason Jones (darkdiplomat)
 *
 * @see MOTDKey
 */
public interface MessageOfTheDayListener {
}
