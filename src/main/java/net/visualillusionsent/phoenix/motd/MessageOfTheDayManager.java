package net.visualillusionsent.phoenix.motd;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.reflect.Method;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Message of the Day container
 * <p/>
 * Loads and sends the Message of the Day on request.
 *
 * @author Jason Jones (darkdiplomat)
 */
public class MessageOfTheDayManager {
    private static final Logger log = LoggerFactory.getLogger("Phoenix-MOTDManager");
    private static final List<String> motdLines;
    private static final List<MOTDParser> motdVars;
    private static final Matcher permMatch = Pattern.compile("\\{permissions:(.)+}.+").matcher("");

    private final File motdFile;

    static {
        motdLines = Collections.synchronizedList(new ArrayList<String>());
        motdVars = Collections.synchronizedList(new ArrayList<MOTDParser>());
    }

    public MessageOfTheDayManager(File motdFile) {
        if (motdFile == null) throw new IllegalArgumentException("MOTD File cannot be set to null");
        this.motdFile = motdFile;
        try {
            loadMOTD();
        } catch (Exception ex) {
            log.error("Failed to read/write Message of the Day from/to the MOTD file.", ex);
        }
    }

    private void loadMOTD() throws IOException {
        if (!motdFile.exists()) {
            if (!motdFile.createNewFile()) throw new IOException("Unable to create MOTD file");
            PrintWriter writer = new PrintWriter(new FileWriter(motdFile));
            writer.println("# (Login) Message of the Day");
            writer.println("# Lines may be prefixed with {permission:<[node] or ![node] or [node]&[node]&![node]>}");
            writer.println("# Examples: {permission:phoenix.motd} {permission:phoenix.world.build&!phoenix.world.spawnbuild}");
            writer.println("# # # # #");
            writer.flush();
            writer.close();
        }
        else {
            FileInputStream fis = new FileInputStream(motdFile);
            Scanner scanner = new Scanner(fis, "UTF-8");

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();

                if (line.startsWith("#")) {
                    continue;
                }
                motdLines.add(line);
            }
            scanner.close();
            fis.close();
        }
    }

    /**
     * Sends the MOTD to a {@link MOTDReceiver}
     *
     * @param motdRec
     *         the {@link MOTDReceiver} who will receive the MOTD
     */
    public void sendMOTD(MOTDReceiver motdRec) {
        if (motdRec == null) {
            log.warn("MOTDReceiver was null, unable to send MOTD so skipping...");
            return;
        }

        synchronized (motdLines) {
            for (String line : motdLines) {
                String toSend = line;
                if (permMatch.reset(toSend).matches()) {
                    String perms = toSend.substring(toSend.indexOf(':') + 1, toSend.indexOf('}'));
                    permsParse:
                    {
                        if (perms.contains("&")) {
                            String[] permissions = perms.split("&");
                            multiParse:
                            {
                                for (String permission : permissions) {
                                    if (permission.charAt(0) == '!' && motdRec.hasPermission(permission.substring(1))) {
                                        break multiParse; // No good, continue to next line
                                    }
                                    else if (!motdRec.hasPermission(permission)) {
                                        break multiParse; // No good, continue to next line
                                    }
                                }
                                break permsParse; // All good, continue regular parsing
                            }
                        }
                        else if (perms.charAt(0) == '!') {
                            if (!motdRec.hasPermission(perms.substring(1))) {
                                break permsParse; // All good, continue regular parsing
                            }
                        }
                        else if (motdRec.hasPermission(perms)) {
                            break permsParse; // All good, continue regular parsing
                        }
                        continue; // No good, continue to next line
                    }
                    toSend = toSend.replace(toSend.substring(0, toSend.indexOf('}') + 1), ""); // Remove permission check substring
                }
                // Parse variables
                synchronized (motdVars) {
                    for (MOTDParser motdp : motdVars) {
                        try {
                            toSend = toSend.replace(motdp.key(), motdp.parse(motdRec));
                        } catch (Exception ex) {
                            log.error("Failed to parse MessageOfTheDay Variable from MOTDOwner: " + motdp.getOwner().getName(), ex);
                        }
                    }
                }

                //Replace Color codes
                toSend = toSend.replaceAll("&([0-9A-FK-ORa-fk-or])", "\u00A7$1");
                motdRec.message(toSend);
            }
        }
    }

    /**
     * Registers a new {@link MessageOfTheDayListener} to be used with the MessageOfTheDay
     *
     * @param listener
     *         the {@link MessageOfTheDayListener} to be added
     * @param owner
     *         the {@link MOTDOwner} (either the server or a plugin)
     * @param force
     *         {@code true} to override existing keys if existant; {@code false} to error out on duplicate keys (recommended)
     */
    public void registerMOTDListener(final MessageOfTheDayListener listener, final MOTDOwner owner, final boolean force) {
        Method[] methods = listener.getClass().getDeclaredMethods();

        for (final Method method : methods) {
            if (!method.isAnnotationPresent(MOTDKey.class)) {
                continue;
            }
            if (method.getReturnType() != String.class) {
                log.warn("You have a MOTD Listener method with invalid return type! (" + method.getName() + ") (Expected: String Got: " + method.getReturnType().getName() + ")");
                continue;
            }
            Class<?>[] params = method.getParameterTypes();
            if (params.length != 1) {
                log.warn("You have a MOTD Listener method with invalid number of arguments! (" + method.getName() + ") (Expected: 1 Got: " + params.length + ")");
                continue;
            }
            if (!MOTDReceiver.class.isAssignableFrom(params[0])) {
                log.warn("You have a MOTD method with invalid argument types! - " + method.getName());
                continue;
            }
            MOTDKey meta = method.getAnnotation(MOTDKey.class);
            MOTDParser motdp = new MOTDParser(meta.value(), owner) {
                @Override
                String parse(MOTDReceiver motdRec) throws Exception {
                    return (String) method.invoke(listener, motdRec);
                }
            };

            // Check for duplicate keys
            for (MOTDParser parser : motdVars) {
                if (meta.value().equals(parser.key()) && !force) {
                    log.warn(owner.getName() + " attempted to register MOTDKey: '" + meta.value() + "' but it is already registered to " + parser.getOwner().getName());
                }
            }
            motdVars.add(motdp);
        }
    }

    /**
     * Removes all of a {@link MOTDOwner}'s {@link MessageOfTheDayListener} methods from the MessageOfTheDay list
     *
     * @param owner
     *         the {@link MOTDOwner} to have {@link MessageOfTheDayListener} methods removed
     */
    public void unregisterMOTDListener(MOTDOwner owner) {
        synchronized (motdVars) {
            Iterator<MOTDParser> motdpItr = motdVars.iterator();
            while (motdpItr.hasNext()) {
                if (motdpItr.next().getOwner() == owner) { // Yes, memory address exact
                    motdpItr.remove();
                }
            }
        }
    }

    /**
     * Reloads the Message of the Day file
     */
    public void reload() {
        motdLines.clear();
        try {
            loadMOTD();
        } catch (Exception ex) {
            log.error("Failed to read/write Message of the Day from/to the motd.txt file.", ex);
        }
    }
}
