package net.visualillusionsent.phoenix.motd;

/**
 * MOTD Receiving interface
 *
 * @author Jason Jones (darkdiplomat)
 */
public interface MOTDReceiver {

    /**
     * Returns the name of the MOTDReceiver
     *
     * @return receiver's name
     */
    String getName();

    /**
     * Sends the MOTD lines to this {@code MOTDReceiver}.
     *
     * @param message
     *         The message to send.
     */
    void message(String message);

    /**
     * Check if the {@code MOTDReceiver} has a permission relating to a MOTDKey.
     *
     * @param node
     *         The node to check for.
     *
     * @return {@code true} if this {@code MOTDReceiver} has the given
     * permission, {@code false} otherwise.
     */
    boolean hasPermission(String node);
}
